# Notes

- https://gitlab.com/joaommpalmeiro/template-react-vite-tailwind
- https://gitlab.com/joaommpalmeiro/template-react-vite

## Commands

```bash
npm install \
react \
react-dom \
&& npm install -D \
@biomejs/biome \
@joaopalmeiro/biome-react-config \
@types/react \
@types/react-dom \
@typescript-eslint/parser \
@vitejs/plugin-react \
autoprefixer \
create-vite-tsconfigs \
eslint \
eslint-plugin-tailwindcss \
npm-run-all2 \
postcss \
sort-package-json \
tailwindcss \
typescript \
vite
```

```bash
rm -rf node_modules/ && npm install
```

```bash
npm config ls -l
```

### Clean slate

```bash
rm -rf dist/ node_modules/
```

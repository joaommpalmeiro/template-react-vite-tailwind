# Template Notes

- https://www.w3.org/WAI/tutorials/page-structure/example/
- Tailwind CSS:
  - https://tailwindcss.com/docs/aspect-ratio
  - https://tailwindcss.com/docs/guides/vite:
    - https://tailwindcss.com/docs/guides/vite#react
  - https://tailwindcss.com/blog/tailwindcss-v4-alpha
    - https://daily.dev/blog/tailwind-css-40-everything-you-need-to-know-in-one-place
    - [Why Isn't tailwindcss v4 a devdependency like v3?](https://github.com/tailwindlabs/tailwindcss/discussions/13854)
    - https://github.com/tailwindlabs/tailwindcss/blob/62de02a37946c48c5fa6cdb800173b54198ebb9b/playgrounds/vite/vite.config.ts: `export default defineConfig({ plugins: [react(), tailwindcss()] })`
  - https://www.npmjs.com/package/tailwindcss
  - https://github.com/tailwindlabs/tailwindcss/blob/next/CHANGELOG.md
  - https://github.com/farm-fe/farm/blob/main/examples/tailwind/.postcssrc
  - https://github.com/postcss/postcss-load-config
  - https://tailwindcss.com/docs/word-break#break-all
  - https://github.com/tailwindlabs/tailwindcss/releases
  - https://flaviocopes.com/fix-unknown-at-rule-tailwind/
  - https://tailwindcss.com/docs/editor-setup:
    - https://github.com/tailwindlabs/tailwindcss-intellisense?tab=readme-ov-file#recommended-vs-code-settings
  - https://github.com/francoismassart/eslint-plugin-tailwindcss/releases
- @typescript-eslint/parser:
  - https://github.com/typescript-eslint/typescript-eslint/releases
  - https://github.com/typescript-eslint/typescript-eslint/blob/v8.11.0/packages/parser/CHANGELOG.md
  - https://typescript-eslint.io/packages/parser/
  - https://typescript-eslint.io/packages/parser#jsxpragma: `Default 'React'`
- https://timjames.dev/blog/the-best-eslint-rules-for-react-projects-30i8
- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#examples
- https://developer.mozilla.org/en-US/docs/Web/HTML/Viewport_meta_tag#viewport_basics
- https://developer.mozilla.org/en-US/docs/Glossary/Doctype
- ESLint:
  - https://github.com/eslint/eslint/releases
  - https://eslint.org/docs/latest/use/configure/configuration-files#typescript-configuration-files
  - https://explorer.eslint.org/
  - https://eslint.org/blog/2024/09/eslint-v9.10.0-released/
  - https://eslint.org/blog/2024/09/eslint-v9.11.0-released/
  - https://eslint.org/blog/2024/10/eslint-v9.12.0-released/
  - https://eslint.org/blog/2024/10/eslint-v9.13.0-released/
- https://github.com/vitejs/vite/tree/v5.4.11/packages/create-vite/template-react-ts
- https://github.com/vitejs/vite/tree/create-vite%405.5.5/packages/create-vite/template-react-ts
- https://eslint.org/docs/latest/use/configure/configuration-files#typescript-configuration-files
- https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html#satisfies
- https://tanstack.com/query/latest/docs/eslint/eslint-plugin-query

## Commands

```bash
npm create vite@5.5.5 react-template -- --template react-ts
```
